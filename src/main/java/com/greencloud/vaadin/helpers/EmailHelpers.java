package com.greencloud.vaadin.helpers;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.greencloud.vaadin.common.Constants;
import com.greencloud.vaadin.dto.EmailDTO;
import com.liferay.portal.kernel.mail.MailMessage;

public class EmailHelpers {

	public static MailMessage createSendableEmail(EmailDTO mail) throws AddressException {
		MailMessage msg = new MailMessage();
		msg.setFrom(new InternetAddress(mail.getFromAddress().toString()));
		msg.setTo(createAddressArray(mail.getToAddress()));
		msg.setSubject(mail.getMessageSubject());
		msg.setBody(mail.getMessageBody());
		msg.setHTMLFormat(true);
		return msg;
	}

	public static InternetAddress[] createAddressArray(String addressString) throws AddressException {
		String[] toAddresses = addressString.split(Constants.ADDRESS_DELIMITER);
		InternetAddress[] addresses = new InternetAddress[toAddresses.length];
		for(int a = 0; a < toAddresses.length; a++) {
			addresses[a]=new InternetAddress(toAddresses[a]);
		}
		
		return addresses;
	}
	
}
