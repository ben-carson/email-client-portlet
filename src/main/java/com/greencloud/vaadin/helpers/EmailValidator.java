package com.greencloud.vaadin.helpers;

import com.greencloud.vaadin.dto.EmailDTO;
import com.greencloud.vaadin.exceptions.EmailAppException;

public class EmailValidator {

	public static void validateEmailDTO(EmailDTO email) throws EmailAppException {
		if(email == null) {
			throw new EmailAppException("Email DTO is null!");
		}
		if(email.getFromAddress() == null || email.getFromAddress().equals("")) {
			throw new EmailAppException("Email DTO's \"From:\" address is empty!");
		}
		if(email.getToAddress() == null || email.getToAddress().equals("")) {
			throw new EmailAppException("Email DTO's \"To:\" address is empty!");
		}
		if(email.getMessageSubject() == null || email.getMessageSubject().equals("")) {
			throw new EmailAppException("Email DTO's \"Subject:\" field is empty!");
		}
		if(email.getMessageBody() == null || email.getMessageBody().equals("")) {
			throw new EmailAppException("Email DTO's \"Body:\" field is empty!");
		}
	}
	
}
