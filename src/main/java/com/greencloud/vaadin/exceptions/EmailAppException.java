package com.greencloud.vaadin.exceptions;

public class EmailAppException extends Exception {

	private static final long serialVersionUID = -6450056563998662310L;
	private String mailException;
	
	public EmailAppException() {
		setMailException("There was an error with the Email Application.");
	}
	
	public EmailAppException(String exception) {
		setMailException(exception);
	}
	
	public String getMailException() {
		return mailException;
	}

	public void setMailException(String mailException) {
		this.mailException = mailException;
	}
	
}
