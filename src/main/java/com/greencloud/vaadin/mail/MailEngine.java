package com.greencloud.vaadin.mail;

import javax.mail.internet.AddressException;

import com.greencloud.vaadin.dto.EmailDTO;
import com.greencloud.vaadin.exceptions.EmailAppException;
import com.greencloud.vaadin.helpers.EmailHelpers;
import com.greencloud.vaadin.helpers.EmailValidator;
import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;

public class MailEngine {
	
	private Log _log = LogFactoryUtil.getLog(MailEngine.class);
	private EmailDTO message;
	private MailMessage liferayMsg;
	
	public MailEngine() {
		_log.debug("MailEngine default constructed.");
		setMessage(new EmailDTO());
		setLiferayMsg(new MailMessage());
	}
	
	public MailEngine(EmailDTO msg) {
		_log.debug("MailEngine argument constructed.");
		try {
			EmailValidator.validateEmailDTO(msg);
			_log.debug("Email DTO is valid.");
		} catch (EmailAppException e) {
			_log.error("Email DTO is not valid.");
			_log.error(e.getMailException());
		}
	}

	public void sendMessage(EmailDTO msg) {
		try {
			EmailValidator.validateEmailDTO(msg);
			_log.debug("Email DTO is valid, create sendable object.");
			liferayMsg = EmailHelpers.createSendableEmail(msg);
			_log.debug("Sendable email object created successfully.");
			MailServiceUtil.sendEmail(liferayMsg);
			_log.debug("Message "+msg.getMessageID()+"-"+msg.getMessageSubject()+" was sent to "+msg.getToAddress());
		} catch (AddressException e) {
			_log.error("A problem occurred while attempting to create a sendable email message.");
			_log.error(e.getMessage());
			e.printStackTrace();
		} catch (EmailAppException e) {
			_log.error("An attempt to send an invalid Email DTO has occurred.");
			_log.error(e.getMailException());
			e.printStackTrace();
		}
	}
	
	public EmailDTO getMessage() {
		return message;
	}
	public void setMessage(EmailDTO message) {
		this.message = message;
	}

	public MailMessage getLiferayMsg() {
		return liferayMsg;
	}
	public void setLiferayMsg(MailMessage liferayMsg) {
		this.liferayMsg = liferayMsg;
	}
	
	
	
}
