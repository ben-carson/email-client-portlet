package com.greencloud.vaadin.common;

public class Constants {

	public static final String ADDRESS_DELIMITER = ",";
	
	public static final int BASE_FONT_SIZE = 14;
	public static final String VAADIN_THEME_NAME = "green-mail";
	
	public static final String INPUT_HEIGHT = "38px";
	public static final String INPUT_WIDTH = "400px";
	
}
