package com.greencloud.vaadin.dto;

import java.io.Serializable;

public class EmailDTO implements Serializable {

	private static final long serialVersionUID = 6838234028197219728L;
	
	private int messageID = 0;
	private int threadID = 0;
	private int threadOrder = 0;
	private MessageType fromAddress = MessageType.OUTAGE;
	private String toAddress = "";
	private String messageSubject = "";
	private String messageBody = "";
	private int typeID = 0;

	public int getMessageID() {
		return messageID;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	
	public int getThreadID() {
		return threadID;
	}
	public void setThreadID(int threadID) {
		this.threadID = threadID;
	}
	
	public int getThreadOrder() {
		return threadOrder;
	}
	public void setThreadOrder(int threadOrder) {
		this.threadOrder = threadOrder;
	}
	
	public MessageType getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(MessageType fromAddress) {
		this.fromAddress = fromAddress;
	}
	
	public String getToAddress() {
		return toAddress;
	}
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}
	
	public String getMessageSubject() {
		return messageSubject;
	}
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
	
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
	public int getTypeID() {
		return typeID;
	}
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}
	
}
