package com.greencloud.vaadin.dto;

public enum MessageType {

	OUTAGE("support@gogreencloud.com"),
	MARKETING("marketing@gogreencloud.com"),
	BILLING("billing@gogreencloud.com");
	
	private String messageType;
	
	MessageType(String messageType) {
		this.messageType = messageType;
	}
	
	@Override
	public String toString() {
		return getMessageType();
	}

	private String getMessageType() {
		return messageType;
	}
	
}
