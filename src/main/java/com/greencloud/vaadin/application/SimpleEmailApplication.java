package com.greencloud.vaadin.application;

import com.greencloud.vaadin.common.Constants;
import com.greencloud.vaadin.components.EmailForm;
import com.vaadin.Application;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SimpleEmailApplication extends Application {

	private static final long serialVersionUID = -154396815851423104L;
	
	@Override
	public void init() {
		buildMainLayout();
		setTheme(Constants.VAADIN_THEME_NAME);
	}
	
	private void buildMainLayout() {
		setMainWindow(new Window("Simple Vaadin Email Client Portlet"));
		
		EmailForm mailForm = new EmailForm();
		getMainWindow().setHeight(500, VerticalLayout.UNITS_PIXELS);
		getMainWindow().setWidth(500, VerticalLayout.UNITS_PIXELS);
		getMainWindow().addComponent(mailForm);
	}
	

}
